# EasyCryptoLib
Version 1.0.0 -- 2019-05-14 -- (c) Antti Juustila

[TOC]

The source code here implements a dynamic library called EasyCryptoLib, which is capable of encrypting and decrypting text using several methods of cryption. Methods supported in the current version are "reverse" and  "matrix".

The library is implemented using C++17 and utilises the Standard Template Library as well as Boost version 1.70.

The library is tested to work on Windows10, Ubuntu as well as Mac OS X 10.15.

The html directory contains Doxygen generated documentation for the library. The included doxyfile can be used to regenerate the docs.

## Building on *nix machines

You need a C++17 c++ compiler and CMake. Install those first. Ninja is the preferred build tool since it also works well on Windows 10 using Microsoft Visual Studio 2019. Ninja comes installed with that tool. In macOS and Ubuntu, install Ninja using your preferred package manager, e.g. Ubuntu: `sudo apt-get install ninja` and on macOS using Homebrew: `brew install ninja`.

Instructions below assume that the compiler, CMake and Ninja have all been installed.

Building is done using CMake. See the included CMakeLists.txt files for the library and the dll's.

The interface to the EasyCryptoLib consists of three files:

* EasyCryptoLib.hpp, the header file used by client apps
* ECMasterHeader.hpp, defining the way library entities are exported and imported in different environments.
* EasyCrytoLib.dylib/.so/.dll+.lib, a shared (dynamic) library clients use at runtime

The header EasyCryptoLibBad.hpp is also exported as part of the library interface for demonstration purposes, but is a *bad* example, the reason why it is not listed above. Please remove this interface class before "seriously" using the library, and rebuild and install the public interface using the  commands as described below.

Assumption is that after building, the interface consisting of these three files is copied to a shared include and library directory in the system and used from there by the client applications. Using CMake, this is done like this on *nix'es (Ubuntu, macOS):

1. mkdir build
2. cd build
3. cmake -GNinja ..
4. ninja
5. sudo ninja install

In Ubuntu and Mac, the directory for the shared hpp file is /usr/local/include, and the directory for the shared library file is /usr/local/lib.

## Building on Windows 10

On Windows, you need -- in addition to git and CMake -- Visual Studio C++ 2019, or something new, which understands CMake. Also it is necessary to **enable Windows developer options** and **Windows subsystem for Linux**. 

Use the Visual Studio Command Prompt for developers to give all the console commands below. The usual Command Prompt that comes with Windows is no good for development purposes.

On Windows, you need to tell the tool chain where to install all the binaries and libraries, and where to look for them when building apps using the libaries.

So, **first** create a place for the libraries and executables, e.g. `C:\bin`. Also, you need to put that directory under this path, which contain binaries (.lib, .dll) into the Windows user's PATH environment variable so that the libraries are found when the executables are started. Assuming `C:\bin` is your path, add these to the Windows 10 user Path environment variables from the Windows Control Panel:

```
C:\bin\lib
C:\bin\bin
```

**Secondly**, when giving the CMake commands, you need to tell where to install the EasyCryto library binaries and headers:

```
mkdir build && cd build
cmake -GNinja -DCMAKE_INSTALL_PREFIX=C:\bin ..  
ninja
ninja install
```

This builds the library and installs the target (Debug version) to the `C:\bin` directory, putting headers under `C:\bin\include`, library file under `C:\bin\lib` and the dll under `C:\bin\bin\` directory.

The library public interface should be now available for the applications using this shared library.

## Generating library documentation

Project includes a doxyfile.in you can use with Doxygen to generate html docs of the library. The documentation inlcudes only the public interface of the library, so it is meant for the users who develop apps using the library. Doxygen must be installed to generate the documentation.

CMakeLists.txt includes documentation building script, so after generating the cmake files, do `ninja doc` and the build directory will contain a `docs` subdirectory with html documentation. Open index.html to view it. 

Publish the html directory to aid the app developers in using the library.


## Using the library from apps

In the client app CMakeLists.txt, use `find_package` to find the library:

```
find_package(EasyCrypto REQUIRED)
```

And then set the include and link options:

```
target_include_directories(${APP_NAME} PUBLIC EasyCrypto::EasyCrypto ... )
target_link_libraries(${APP_NAME} EasyCrypto::EasyCrypto ...)
```
In the client app source files (.cpp usually), include the public header API of the library, and then use the library public API:

```
#include <EasyCrypto/EasyCryptoLib.hpp>

...
EasyCryptoLib::Result r = EasyCryptoLib::encrypt(plainText, encrypted, method);
```


## License

The library is published under LGPL.  Please see the provided [LICENSE](LICENSE) file.

(C) Antti Juustila, 2016-2020 All rights reserved.
antti dot juustila at oulu.fi
