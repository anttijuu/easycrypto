# EasyCryptoConsole #

EasyCryptoConsole is a console app you can use to test the EasyCryptoLib library.

## Dependencies

* EasyCryptoLib shared library

Build and install the library first. See the readme from that project.

## Building EasyCryptoConsole

Then build the console using CMake and Ninja (*nixes):

```
cd EasyCryptoConsole
mkdir build && cd build
cmake -GNinja ..
ninja
``` 

**Now on Windows** you need to tell where to find the installed library EasyCryptoLib):

```
cd EasyCryptoConsole
mkdir build
cd build
cmake -GNinja -DCMAKE_PREFIX_PATH=C:\bin ..
ninja
```

Note that there is no need to install the console using `ninja install` since the executable may be executed from the build directory. Obviously if you want, you can copy the binary to C:\bin\bin if you wish and execute it from there.

After this, you can start the console: `./CryptoConsole`.

The app will test the EasyCryptoLib usin a good API and also a Bad API. Check out the code for more details. 

Modify the code for example to test additional encryption methods you have implemented.

## License

The library is published under LGPL.  Please see the provided [LICENSE](LICENSE) file.

(C) Antti Juustila, 2016-2020 All rights reserved.
antti dot juustila at oulu.fi
