# EasyCrypto #

This system was a course project in the Study Program for Information Processing Science, University of Oulu. Course module where this project was used focused on building and using libraries and APIs.

Themes handled through this project are:

* dynamic (shared) libraries in C++ and the binary compatibility issues there (demo);
* API headers, building a library and distributing it;
* an API exposed over the internet, using udp sockets/boost and JSON;
* tools for making cross-platform libraries using different compilers in different OSes; CMake

Project includes a "bad" API to the library, and also a better one, which is more robust concerning API changes and binary compatibility.

Student task is to study the binary compatibility issues, develop a new crypto method (as simple as possible) and design the JSON API for the networked solution, implement and test it with the client and server.


## Building

Build the components of the system in this order:

1. EasyCryptoLib -- it needs to be build and installed first. See readme in this directory for details.
1. EasyCryptoConsole -- a console app that can be used in testing EasyCryptoLib
1. EasyCryptoServer -- a server app that uses EasyCryptoLib to provide crypto services to client apps.
1. EasyCryptoClient -- a client app communicating with the server over UDP using JSON as a transport data format.

Mind that the Client and the Server miss code *you* need to implement. First you have to

1. Design an API between the client and server, using UDP and JSON
1. Implement the designed API both in Client and Server, looking at missing parts and code comments instructing where to insert the missing code.

## License

The system is published under LGPL.  Please see the provided [LICENSE](LICENSE) file.

(C) Antti Juustila, 2016-2020 All rights reserved.
antti dot juustila at oulu.fi
