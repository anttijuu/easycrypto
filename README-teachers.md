# EasyCrypto #

This readme is meant for teachers only. **REMOVE** this file when giving the project to students. 

Also **check out instructions below** what else to remove from the version given to students as a starting point for their work.

This system was a course project in the Study Program for Information Processing Science, University of Oulu. Course module where this project was used focused on building and using libraries and APIs.

Themes handled through this project are:

* dynamic (shared) libraries in C++ and the binary compatibility issues there (demo);
* API headers, building a library and distributing it;
* an API exposed over the internet, using udp sockets/boost and JSON;
* tools for making cross-platform libraries using different compilers in different OSes; CMake

Project includes a "bad" API to the library, and also a better one, which is more robust concerning API changes and binary compatibility.

Student task is to study the binary compatibility issues, develop a new crypto method (as simple as possible) and design the JSON API for the networked solution, implement and test it with the client and server.

See also a [demonstration of the same app](https://github.com/anttijuu/PluginDllDemo) but the dll implementing a plugin dll architecture. Differences are in the Lib and plugin dll directories, client and server being similar to one here.


## Demo: how to view the bad API crashing

This demonstrates how a library can be changed so that it breaks binary compatibility causing an app to use the library to crash if app has not been build against the new lib and the API of the lib is not well designed.

A Finnish demo video of how the bad version of the API breaks can be seen in [YouTube](https://youtu.be/XKpXYxJDslU).

How to execute this demo:

1. Comment out Matrix encryption from the project totally (comment source files out from Lib project CMakeLists.txt) and comment out usage of the Matrix from the EasyCryptoLib.cpp and EasyCryptoBad.hpp/.cpp. 
2. Build the lib and install it (see Building below)
3. Build the CryptoClientConsole app
4. Use the good and bad lib API from the CryptoClientConsole app
5. Now "add" new functionality to the Lib by taking the Matrix method back in by removing comments. If you forget to remove some comments you will probably see build errors so be careful.
6. build the Lib (and the lib only!) and install it (see Building below)
7. DO NOT BUILD the CryptoClientConsole app. This simulates the situation that some *other* developer has updated their app which *also* uses the same Lib, has updated it but *CryptoClientConsole app has not yet been rebuild* against the new Lib (because the devs of Console app are slow or on vacation, what ever).
7. Run the CryptoClientConsole app to see how when using the EasyCryptoBad API, the console app crashes, but NOT using the EasyCryptoLib (good) api.

Study why using the bad API class the app crashed, and study why library developers must be very aware of binary compatibility issues when updating a published library.

### Editing the code before giving it to the students

*This section relates to the coursework where students were given a skeleton of the code to fill out missing parts.*

**IMPORTANT** On the client app side, teacher must remove the implementation of handleXxxx and other methods in the Client code marked with comments:

```
   // Remove all code below in this method from code given to students.
```

**IMPORTANT** On the server app side, teacher must remove the implementation of handling client responses. Look for comments starting `// Remove` for the code that must be removed before giving the code to the students.

Also check code to remove from the header files of Client and Server.

Only then the skeleton code of Client, Lib and Server can be given to the students. Do *not* share the code from Bitbucket or similar, since the git *version history* enables the students to go back to the version which *includes* these implementations. Copy the CMake and source files only, excluding the hidden .git directories where the version history is also located.

It is then the student's responsibility to: 

1. design the protocol of JSON message structures that are send and received by the client and server to enable encryption/decryption distributed way, and
2. implement constructing and parsing the messages using jsoncpp library, and
3. implement the request handling in client and server side so that everything works, and
4. add the third encryption method to the library without breaking binary compatibility (the bad library API can be dropped at this point since it is just an example of how *not* to do things). Method could be e.g. rot13 or similar. 


## Dependencies

Check out the project components for external depencies such as Boost and nlohmann::json and how to get, build and install those before going into the build processes of components of this project.

## Building EasyCrypto

Check out the [README.md](README.md) for build instructions.

## License

The system is published under LGPL.  Please see the provided [LICENSE](LICENSE) file.

(C) Antti Juustila, 2016-2020 All rights reserved.
antti dot juustila at oulu.fi
